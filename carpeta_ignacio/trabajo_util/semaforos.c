#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/wait.h>

#define sem_wait(SEM_NUM)            \
    operacion.sem_num = SEM_NUM; \
    operacion.sem_op = -1;       \
    semop(semid, &operacion, 1);
#define sem_post(SEM_NUM)            \
    operacion.sem_num = SEM_NUM; \
    operacion.sem_op = 1;        \
    semop(semid, &operacion, 1);
#define N 100 // Cantidad de espacios en la memoria compartida

struct buffer
{
    int id;
    struct timeval tiempo;
    double dato_leido;
};

struct timeval inicio;

union semun
{
    int val;
    struct semid_ds *buf;
    unsigned short *array;
    struct seminfo *__buf;
};

int main()
{
    // Obtiene el tiempo inicial
    gettimeofday(&inicio, NULL);

    // Obtiene las claves para los semaforos y el buffer
    key_t shmkey = ftok("/bin/ls", 0x1234);
    key_t semkey = ftok("/bin/ls", 0x5678);

    union semun arg;
    struct sembuf operacion;
    operacion.sem_flg = 0;

    // Crear semaforos y memoria compartida
    int shmid = shmget(shmkey, N * sizeof(struct buffer), IPC_CREAT | 0666);
    int semid = semget(semkey, 3, IPC_CREAT | 0666);

    // Configura los semaforos 0 y 1 en 1(libre), el semaforo 2 en 0(ocupado)
    arg.val = 1;
    semctl(semid, 0, SETVAL, arg);
    semctl(semid, 1, SETVAL, arg);
    arg.val = 0;
    semctl(semid, 2, SETVAL, arg);

    // Obtiene la memoria compartida
    struct buffer *buffer = shmat(shmid, NULL, 0);

    // Crea una estructura condicional para el proceso hijo
    pid_t pid;
    if (pid = fork())
    {
        // El proceso padre abre el archivo datos.dat en modo rb
        FILE *archivo = fopen("datos.dat", "rb");
        if (archivo == NULL)
        {
            printf("Error al abrir el archivo\n");
            exit(1);
        }
        int end = 0, j = 0;
        while (!end)
        {
            // Espera al semaforo 0
            sem_wait(0);
            for (int i = 0; i < N / 2 && !end; i++)
            {
                // Si no se llego al final del archivo, lee una linea
                char linea[16];
                if (fread(linea, 16 * sizeof(char), 1, archivo) == 1)
                {
                    // Elimina el ultimo caracter del string y los espacios en blanco del principio
                    linea[16] = '\0';
                    int k = 0;
                    while (linea[k] == ' ')
                        k++;
                    strcpy(linea, linea + k);
                    // Convierte el string a double
                    buffer[i].dato_leido = atof(linea);
                    // Obtiene el tiempo actual
                    gettimeofday(&buffer[i].tiempo, NULL);
                    // Calcula el tiempo relativo
                    buffer[i].tiempo.tv_sec -= inicio.tv_sec;
                    buffer[i].tiempo.tv_usec -= inicio.tv_usec;
                    if (buffer[i].tiempo.tv_usec < 0)
                    {
                        buffer[i].tiempo.tv_usec += 1000000;
                        buffer[i].tiempo.tv_sec--;
                    }
                    // Incrementa el id
                    buffer[i].id = j++;
                }
                else
                {
                    // Si se llego al final del archivo, setea el id en -1 y el flag de fin
                    buffer[i].id = -1;
                    end = 1;
                }
            }
            // Libera el semaforo 0
            sem_post(0);
            
            // Espera al semaforo 1
            sem_wait(1);
            for (int i = N / 2; i < N && !end; i++)
            {
                // Si no se llego al final del archivo, lee una linea
                char linea[16];
                if (fread(linea, 16 * sizeof(char), 1, archivo) == 1)
                {
                    // Elimina el ultimo caracter del string y los espacios en blanco del principio
                    linea[16] = '\0';
                    int k = 0;
                    while (linea[k] == ' ')
                        k++;
                    strcpy(linea, linea + k);
                    // Convierte el string a double
                    buffer[i].dato_leido = atof(linea);
                    // Obtiene el tiempo actual
                    gettimeofday(&buffer[i].tiempo, NULL);
                    // Calcula el tiempo relativo
                    buffer[i].tiempo.tv_sec -= inicio.tv_sec;
                    buffer[i].tiempo.tv_usec -= inicio.tv_usec;
                    if (buffer[i].tiempo.tv_usec < 0)
                    {
                        buffer[i].tiempo.tv_usec += 1000000;
                        buffer[i].tiempo.tv_sec--;
                    }
                    // Incrementa el id
                    buffer[i].id = j++;
                }
                else
                {
                    // Si se llego al final del archivo, setea el id en -1 y el flag de fin
                    buffer[i].id = -1;
                    end = 1;
                }
            }
            // Libera el semaforo 1
            sem_post(1);

            // Espera al semaforo 2 para la sincronizacion entre procesos
            sem_wait(2);
        }
        fclose(archivo);
        // Libera la memoria compartida
        shmdt(buffer);
        // Espera a que termine el proceso hijo
        wait(NULL);
        // Elimina la memoria compartida
        shmctl(shmid, IPC_RMID, NULL);
        // Elimina el semaforo
        semctl(semid, 0, IPC_RMID);
    }
    else
    {
        // El proceso hijo abre el archivo csv en modo w
        FILE *archivo = fopen("datos.csv", "w");
        if (archivo == NULL)
        {
            printf("Error al abrir el archivo\n");
            exit(1);
        }

        // Imprime el encabezado del archivo csv
        fprintf(archivo, "id,tiempo,dato\n");

        int end = 0;
        while (!end)
        {
            // Espera al semaforo 0
            sem_wait(0);
            for (int i = 0; i < N / 2 && !end; i++)
            {
                // Si el buffer no esta vacio, imprime los datos en pantalla y en el archivo
                if (buffer[i].id != -1)
                {
                    printf("%d,%ld.%06ld,%f\n", buffer[i].id, buffer[i].tiempo.tv_sec, buffer[i].tiempo.tv_usec, buffer[i].dato_leido);
                    fprintf(archivo, "%d,%ld.%06ld,%f\n", buffer[i].id, buffer[i].tiempo.tv_sec, buffer[i].tiempo.tv_usec, buffer[i].dato_leido);
                }
                else
                {
                    end = 1;
                }
            }
            // Libera el semaforo 0
            sem_post(0);

            // Libera el semaforo 2 (sincronizacion entre procesos)
            sem_post(2);

            // Espera el semaforo 1
            sem_wait(1);
            for (int i = N / 2; i < N && !end; i++)
            {
                // Si el buffer no esta vacio, imprime los datos en pantalla y en el archivo
                if (buffer[i].id != -1)
                {
                    printf("%d,%ld.%06ld,%f\n", buffer[i].id, buffer[i].tiempo.tv_sec, buffer[i].tiempo.tv_usec, buffer[i].dato_leido);
                    fprintf(archivo, "%d,%ld.%06ld,%f\n", buffer[i].id, buffer[i].tiempo.tv_sec, buffer[i].tiempo.tv_usec, buffer[i].dato_leido);
                }
                else
                {
                    end = 1;
                }
            }
            // Libera el semaforo 1
            sem_post(1);
        }
        // Cierra el archivo csv
        fclose(archivo);
        // Libera la memoria compartida
        shmdt(buffer);
    }

    return 0;
}