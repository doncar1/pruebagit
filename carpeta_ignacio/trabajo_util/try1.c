#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <fcntl.h>  // Para O_RDONLY
#include <unistd.h> // Para read y close


#define N 100 // Tamaño del buffer


int in = 0;     // Índice de escritura en el buffer
int out = 0;    // Índice de lectura en el buffer
char buffer[N]; // Buffer compartido
int productor_activo = 1;       // Inicialmente, el productor 1 comienza
int trabajar = 1;               // Inicialmente trabaja.

sem_t empty, full;  // Semáforos para sincronización

// Función del productor
void *producer(void *arg) {

        while(trabajar){
                if (productor_activo == 1) {
                        // Trabaja el hilo P1.
                        printf("Hilo P1 Activo. \n");

                        // Abre el archivo 1 para lectura
                        int fd = open("archivo1", O_RDONLY);
                        if (fd < 0) {
                            perror("Error al abrir archivo 1");
                            exit(1);
                        }

                        // Lee el archivo y almacena los datos en el buffer
/*                      ssize_t bytesRead = read(fd, &buffer[in], (N/2) - in);
                        if (bytesRead > 0) {
                            in += bytesRead;
                        }
*/
                        int lineCount = 0;

                        ssize_t bytesRead;
                        while ((bytesRead = read(fd, &buffer[in], 1) > 0)) {
                                in++;
                                if (buffer[in - 1] == '\n') {
                                        lineCount++;
                                }

                                if (lineCount >= 50) {
                                break;  // Si se alcanza el límite de 50 líneas, detén la lectura.
                                }
                        }

                        close(fd);

                        printf("Contenido del buffer:\n");
                        for (int i = 0; i < in; i++) {
                            printf("buffer[%d] = %c\n", i, buffer[i]);
                        }

                        printf("Oración completa:\n%s\n", buffer);

                        sleep(100);
                }
                else if(productor_activo == 2){
                        // Trabaja el hilo P2.
                        printf("Hilo P2 Activo. \n");
                }
        }

        pthread_exit(NULL);
}

void intercambiar_productor() {
        // Si productor_activo es 1, se cambia a 2, y si es 2, se cambia a 1.
        // Se imprime en pantalla el cambio.
        printf("Proceso productor P%d estaba activo.\n", (productor_activo == 1) ? 1 : 2);
        productor_activo = (productor_activo == 1) ? 2 : 1;
        printf("Proceso productor P%d ahora está activo.\n", (productor_activo == 1) ? 1 : 2);
}


void SignalHandler(int sig) {
    switch (sig) {
        case SIGUSR1:
                printf("Señal SIGUSR1 recibida. Intercambiando proceso productor.\n");
                intercambiar_productor();
                break;
        case SIGTERM:   
                printf("Señal SIGTERM recibida. Terminando el proceso. \n");
                trabajar = 0;
                break;
        // Se puede agregar otra señal mas si es necesario.
    }
}


int main() {

        pthread_t producerThreadP1, producerThreadP2;

        // Inicializar semáforos
        sem_init(&empty, 0, N); 
        sem_init(&full, 0, 0);

        // Signal Handler.
        signal(SIGUSR1, SignalHandler);
        signal(SIGTERM, SignalHandler);

        // Crear hilos de los productores
        pthread_create(&producerThreadP1, NULL, producer, NULL);
        pthread_create(&producerThreadP2, NULL, producer, NULL);

        // Esperar a que los hilos terminen
        pthread_join(producerThreadP1, NULL);
        pthread_join(producerThreadP2, NULL);

        sem_destroy(&empty);  // Destruir semáforos
        sem_destroy(&full);

        return 0;
}




