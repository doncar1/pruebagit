#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/shm.h>
#include <sys/wait.h>

#define N 100
#define BUFFER1 0
#define BUFFER2 1
#define READ 1
#define WRITE 2
#define send(buff, mode)            \
    msg.mtype = mode;               \
    if (buff == BUFFER1)            \
        msgsnd(msg1id, &msg, 0, 0); \
    else                            \
        msgsnd(msg2id, &msg, 0, 0);
#define recv(buff, mode)                  \
    if (buff == BUFFER1)                  \
        msgrcv(msg1id, &msg, 0, mode, 0); \
    else                                  \
        msgrcv(msg2id, &msg, 0, mode, 0);

struct buffer
{
    int id;
    struct timeval tiempo;
    double dato_leido;
};

struct timeval inicio;

typedef struct
{
    long mtype; // 0 disponible para leer, 1 disponible para escribir
} msg;

int main()
{
    gettimeofday(&inicio, NULL);

    // Clave para la memoria compartida
    key_t shmkey = ftok("/bin/ls", 0x1234);

    // Claves para las colas de mensajes
    key_t msg1key = ftok("/bin/ls", 0x5678);
    key_t msg2key = ftok("/bin/ls", 0x9012);

    // Crea la memoria compartida
    int shmid = shmget(shmkey, N * sizeof(struct buffer), 0666 | IPC_CREAT);

    // Cola de mensajes del buffer 1
    int msg1id = msgget(msg1key, 0666 | IPC_CREAT);
    // Cola de mensajes del buffer 2
    int msg2id = msgget(msg2key, 0666 | IPC_CREAT);

    // Se obtiene la memoria compartida
    struct buffer *buffer = (struct buffer *)shmat(shmid, NULL, 0);

    msg msg;

    // Crea una estructura condicional para el proceso hijo
    pid_t pid;
    if (pid = fork())
    {
        // El proceso padre abre el archivo datos.dat en modo rb
        FILE *archivo = fopen("datos.dat", "rb");
        if (archivo == NULL)
        {
            printf("Error al abrir el archivo\n");
            exit(1);
        }
        int end = 0, j = 0;
        while (!end)
        {
            // Espera un mensaje de la cola del buffer 1 para escritura
            recv(BUFFER1, WRITE);
            for (int i = 0; i < N / 2 && !end; i++)
            {
                // Si no se llego al final del archivo, lee una linea
                char linea[16];
                if (fread(linea, 16 * sizeof(char), 1, archivo) == 1)
                {
                    // Elimina el ultimo caracter del string y los espacios en blanco del principio
                    linea[16] = '\0';
                    int k = 0;
                    while (linea[k] == ' ')
                        k++;
                    strcpy(linea, linea + k);
                    // Convierte el string a double
                    buffer[i].dato_leido = atof(linea);
                    // Obtiene el tiempo actual
                    gettimeofday(&buffer[i].tiempo, NULL);
                    // Calcula el tiempo relativo
                    buffer[i].tiempo.tv_sec -= inicio.tv_sec;
                    buffer[i].tiempo.tv_usec -= inicio.tv_usec;
                    if (buffer[i].tiempo.tv_usec < 0)
                    {
                        buffer[i].tiempo.tv_usec += 1000000;
                        buffer[i].tiempo.tv_sec--;
                    }
                    // Incrementa el id
                    buffer[i].id = j++;
                }
                else
                {
                    // Si se llego al final del archivo, setea el id en -1 y el flag de fin
                    buffer[i].id = -1;
                    end = 1;
                }
            }
            // Envia un mensaje de la cola del buffer 1 para lectura
            send(BUFFER1, READ);

            // Espera un mensaje de la cola del buffer 2 para escritura
            recv(BUFFER2, WRITE);
            for (int i = N / 2; i < N && !end; i++)
            {
                // Si no se llego al final del archivo, lee una linea
                char linea[16];
                if (fread(linea, 16 * sizeof(char), 1, archivo) == 1)
                {
                    // Elimina el ultimo caracter del string y los espacios en blanco del principio
                    linea[16] = '\0';
                    int k = 0;
                    while (linea[k] == ' ')
                        k++;
                    strcpy(linea, linea + k);
                    // Convierte el string a double
                    buffer[i].dato_leido = atof(linea);
                    // Obtiene el tiempo actual
                    gettimeofday(&buffer[i].tiempo, NULL);
                    // Calcula el tiempo relativo
                    buffer[i].tiempo.tv_sec -= inicio.tv_sec;
                    buffer[i].tiempo.tv_usec -= inicio.tv_usec;
                    if (buffer[i].tiempo.tv_usec < 0)
                    {
                        buffer[i].tiempo.tv_usec += 1000000;
                        buffer[i].tiempo.tv_sec--;
                    }
                    // Incrementa el id
                    buffer[i].id = j++;
                }
                else
                {
                    // Si llegó al final del archivo, setea el id en -1 y el flag de fin
                    buffer[i].id = -1;
                    end = 1;
                }
            }
            // Envia un mensaje de la cola del buffer 2 para lectura
            send(BUFFER2, READ);
        }
        fclose(archivo);
        // Espera a que termine el proceso hijo
        wait(NULL);
        // Libera la memoria compartida
        shmdt(buffer);
        // Elimina la memoria compartida
        shmctl(shmid, IPC_RMID, NULL);
        // Elimina las cola de mensajes
        msgctl(msg1id, IPC_RMID, NULL);
        msgctl(msg2id, IPC_RMID, NULL);
    }
    else
    {
        // El proceso hijo abre el archivo csv en modo w
        FILE *archivo = fopen("datos.csv", "w");
        if (archivo == NULL)
        {
            printf("Error al abrir el archivo\n");
            exit(1);
        }

        // Imprime el encabezado del archivo csv
        fprintf(archivo, "id,tiempo,dato\n");

        // Envia un mensaje de la cola del buffer 1 y 2 para escritura
        send(BUFFER1, WRITE);
        send(BUFFER2, WRITE);

        int end = 0;
        while (!end)
        {
            // Espera un mensaje de la cola del buffer 1 para lectura
            recv(BUFFER1, READ);
            for (int i = 0; i < N / 2 && !end; i++)
            {
                // Si el buffer no esta vacio, imprime los datos en pantalla y en el archivo
                if (buffer[i].id != -1)
                {
                    printf("%d,%ld.%06ld,%f\n", buffer[i].id, buffer[i].tiempo.tv_sec, buffer[i].tiempo.tv_usec, buffer[i].dato_leido);
                    fprintf(archivo, "%d,%ld.%06ld,%f\n", buffer[i].id, buffer[i].tiempo.tv_sec, buffer[i].tiempo.tv_usec, buffer[i].dato_leido);
                }
                else
                {
                    end = 1;
                }
            }
            // Envia un mensaje de la cola del buffer 1 para escritura
            send(BUFFER1, WRITE);

            // Espera un mensaje de la cola del buffer 2 para lectura
            recv(BUFFER2, READ);
            for (int i = N / 2; i < N && !end; i++)
            {
                // Si el buffer no esta vacio, imprime los datos en pantalla y en el archivo
                if (buffer[i].id != -1)
                {
                    printf("%d,%ld.%06ld,%f\n", buffer[i].id, buffer[i].tiempo.tv_sec, buffer[i].tiempo.tv_usec, buffer[i].dato_leido);
                    fprintf(archivo, "%d,%ld.%06ld,%f\n", buffer[i].id, buffer[i].tiempo.tv_sec, buffer[i].tiempo.tv_usec, buffer[i].dato_leido);
                }
                else
                {
                    end = 1;
                }
            }
            // Envia un mensaje de la cola del buffer 2 para escritura
            send(BUFFER2, WRITE);
        }
        // Cierra el archivo csv
        fclose(archivo);
        // Se libera la memoria compartida
        shmdt(buffer);
    }

    return 0;
}