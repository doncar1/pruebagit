#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <fcntl.h>  // Para O_RDONLY
#include <unistd.h> // Para read y close


#define N 25 // Máximo número de lineas.
#define M 300 // Tamaño máximo de línea.

pthread_t P1, P2, C;
pthread_cond_t cond;
pthread_mutex_t mutex;


int  estoy_en_matriz1 = 1;
char matriz1[N][M];             // Buffer1 compartido
char matriz2[N][M];             // Buffer2 compartido
int turno = 1;                  // Inicialmente, el productor 1 comienza
int trabajar = 1;
int fila =0;               
int fila_aux=0;              
int linea_actual=0;
FILE *archivo;
FILE *archivo2;
sem_t vacio, lleno;  // Semáforos para sincronización



// Función del productor
void *productor1(void *arg) {
    
    archivo = fopen("pg2000.txt", "r");  // Abre el archivo en modo lectura
    
    while(trabajar){

        pthread_mutex_lock(&mutex);

        while (turno != 1) {
                pthread_cond_wait(&cond, &mutex);
        }

        fila=0;
        if(fila_aux >= N){
        fila_aux=0;
        }
        fila=fila+fila_aux;
        
        if (archivo == NULL) {
            perror("No se pudo abrir el archivo");
            exit(1);
        }

        if(estoy_en_matriz1==1){

            while (fila + linea_actual < N + linea_actual && fgets(matriz1[fila], sizeof(matriz1[fila]), archivo) != NULL && turno == 1 ) {
                // Elimina el carácter de nueva línea si existe al final de la línea
                char *p = strchr(matriz1[fila], '\n');
                if (p != NULL) {
                    *p = '\0';
                }
                printf("P1 escribe en matiz 1, linea %d: %s\n", fila+1, matriz1[fila]);
                usleep(5e5);
                fila++;
                fila_aux++;
                linea_actual++;    
            } 

            if(turno==1|| fila==N){    
                estoy_en_matriz1=0;
            }
        }

        fila=0;
        if(fila_aux >= N){
        fila_aux=0;
        }
        fila=fila+fila_aux;

        while (fila + linea_actual < N + linea_actual && fgets(matriz2[fila], sizeof(matriz2[fila]), archivo) != NULL && turno == 1 ) {
            // Elimina el carácter de nueva línea si existe al final de la línea
            char *p = strchr(matriz2[fila], '\n');
            if (p != NULL) {
                *p = '\0';
            }
            printf("P1 escribe en matriz 2, linea %d: %s\n", fila+1, matriz2[fila]);
            usleep(5e5);
            fila++;
            fila_aux++;
            linea_actual++;
        } 
        
        if(turno==1 || fila==N){    
            estoy_en_matriz1=1;
        }

        pthread_cond_signal(&cond);
        pthread_mutex_unlock(&mutex); 

    }
    fclose(archivo);  // Cierra el archivo
    pthread_exit(NULL);
}

void *productor2(void *arg) {
    printf("\n");
    while(trabajar){

        pthread_mutex_lock(&mutex); 

        while (turno != 2) {
            pthread_cond_wait(&cond, &mutex);
        }

        fila=0;
        if(fila_aux >= N){
        fila_aux=0;
        }
        fila=fila+fila_aux;

        if(estoy_en_matriz1==1){

            while (fila + linea_actual < N + linea_actual && fgets(matriz1[fila], sizeof(matriz1[fila]), archivo) != NULL && turno == 2) {
                // Elimina el carácter de nueva línea si existe al final de la línea
                char *p = strchr(matriz1[fila], '\n');
                if (p != NULL) {
                    *p = '\0';
                }
                printf("P2 escribe matriz 1, linea %d: %s\n", fila+1, matriz1[fila]);
            
                usleep(5e5);
                fila++;
                fila_aux++;
                linea_actual++;
            }

            if(turno==2|| fila==N){    
                estoy_en_matriz1=0;
            }
        }

    

        fila=0;
        if(fila_aux >= N){
            fila_aux=0;
        }

        fila=fila+fila_aux;

        while (fila + linea_actual < N + linea_actual && fgets(matriz2[fila], sizeof(matriz2[fila]), archivo) != NULL && turno == 2) {
            // Elimina el carácter de nueva línea si existe al final de la línea
            char *p = strchr(matriz2[fila], '\n');
            if (p != NULL) {
                *p = '\0';
            }
            printf("P2 escribe en matriz 2, linea %d: %s\n", fila+1, matriz2[fila]);
            usleep(5e5);
            fila++;
            fila_aux++;
            linea_actual++;
        } 

        if(turno==2 || fila==N){    
            estoy_en_matriz1=1;
        }

        pthread_cond_signal(&cond);
        pthread_mutex_unlock(&mutex);  

    
    }    
    fclose(archivo);  // Cierra el archivo
    pthread_exit(NULL);
}


void *consumidor(void *arg) {

    while(trabajar){
    }
    pthread_exit(NULL);
}

void cambiar_productor() 
{
   if (turno == 1) {
        turno = 2;
    } else {
        turno = 1;
    }
    pthread_cond_signal(&cond);
}


void SignalHandler(int sig) {
    switch (sig) {
        case SIGUSR1:
                printf("Señal SIGUSR1 recibida. Intercambiando proceso productor.\n");
                cambiar_productor();
                break;
        case SIGTERM:   
                printf("Señal SIGTERM recibida. Terminando el proceso. \n");
                trabajar = 0;
                break;
        // Se puede agregar otra señal mas si es necesario.
    }
}


int main() {

        pthread_cond_init(&cond, NULL);
        pthread_mutex_init(&mutex, NULL);

        // Inicializar semáforos
        sem_init(&vacio, 0, N); 
        sem_init(&lleno, 0, 0);

        // Signal Handler.
        signal(SIGUSR1, SignalHandler);
        signal(SIGTERM, SignalHandler);

        // Crear hilos de los productores
        pthread_create(&P1, NULL, productor1, NULL);
        pthread_create(&P2, NULL, productor2, NULL);

        // Crear hilo consumidor.
        pthread_create(&C, NULL, consumidor, NULL);
       
        // Esperar a que los hilos terminen
        pthread_join(P1, NULL);
        pthread_join(P2, NULL);
        pthread_join(C, NULL);


        // Destruir semáforos
        sem_destroy(&vacio); 
        sem_destroy(&lleno);

        pthread_cond_destroy(&cond);
        pthread_mutex_destroy(&mutex);

        return 0;
}