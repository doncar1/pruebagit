#include <stdio.h> // Para funciones de entrada/salida como printf y fopen
#include <stdlib.h> // Para funciones de manejo de memoria como malloc
#include <pthread.h> // Para funciones de manejo de hilos como pthread_create y pthread_join
#include <unistd.h> // Para la función sleep
#include <signal.h> // Para funciones de manejo de señales como signal

#define BUFFER_TAM 2 // Define el tamaño del buffer
#define LINEAMAX 1024 // Define el tamaño máximo de una línea

char buffer[BUFFER_TAM][LINEAMAX]; // Declara el buffer como una matriz de caracteres
int in = 0; // Variable para escribir en el buffer
int out = 0; // Variable para leer del buffer
char produc_actual = '1'; // Variable para llevar la cuenta del productor actual
int termina = 0; // Variable para indicar si los productores han terminado

pthread_mutex_t mutex; // Declara un mutex para sincronizar el acceso al buffer
pthread_cond_t cond; // Declara una variable de condición para señalar cuando el buffer está vacío o lleno

void *productor(void *arg) { // Funcion que ejecutan los hilos productores
    char *id = (char *)arg; // Obtiene el ID del productor
    FILE *file = fopen("pg2001.txt", "r"); // Abre el archivo de entrada para lectura
    char linea[LINEAMAX-5]; // Buffer para almacenar una línea del archivo, se resta 5 para incluir al "P1: " sin superar tamaño de buffer

    while (fgets(linea, sizeof(linea), file)) { // Lee una línea del archivo
        pthread_mutex_lock(&mutex); // Bloquea el mutex

        if (produc_actual == *id) { // Verifica si este productor esta activo
            while ((in + 1) % BUFFER_TAM == out) { // Si el buffer esta lleno, espera hasta que haya espacio
                pthread_cond_wait(&cond, &mutex);
            }

            snprintf(buffer[in], LINEAMAX, "P%c: %s", *id, linea); // Escribe la línea en el buffer. 

            in = (in + 1) % BUFFER_TAM; // Avanza el índice de escritura

            pthread_cond_signal(&cond); // Señala al consumidor que hay datos en el buffer
        }

        pthread_mutex_unlock(&mutex);

        sleep(1); // Duerme durante un segundo. Esto asegura que solo se produce una linea por segundo
    }

    fclose(file); // Cierra el archivo de entrada. Esto es para liberar los recursos del sistema que se utilizan al abrir un archivo
    pthread_mutex_lock(&mutex);
    termina++; // Incrementa 'termina' cuando un productor ha terminado
    pthread_cond_signal(&cond); // Señala al consumidor en caso de que este esperando
    pthread_mutex_unlock(&mutex);
    return NULL;
}

void *consumidor(void *arg) { // Funcion que ejecuta el hilo consumidor
    FILE *file = fopen("salida.txt", "w"); // Abre el archivo de salida para escritura

    while (1) { 
        pthread_mutex_lock(&mutex); 

        while (in == out && termina < 2) { // Espera si el buffer esta vacío y no todos los productores hayan terminado
            pthread_cond_wait(&cond, &mutex);
        }
        if (in == out && termina == 2) { // Si el buffer esta vacío y todos los productores han terminado, entonces termina
            pthread_mutex_unlock(&mutex);
            break;
        }

        printf("%s", buffer[out]); // Imprime la linea en la consola
        fputs(buffer[out], file); // Escribe la linea en el archivo de salida
        out = (out + 1) % BUFFER_TAM; // Avanza el indice de lectura

        pthread_cond_signal(&cond); // Señala al productor que hay espacio en el buffer

        pthread_mutex_unlock(&mutex); // Desbloquea el mutex
    }

    fclose(file); // Cierra el archivo de salida
    return NULL;
}

void mane_signal(int signal) { // Manejador para la señal SIGUSR1
    pthread_mutex_lock(&mutex);
    if (produc_actual == '1') { 
        produc_actual = '2'; 
    } else { 
        produc_actual = '1'; 
    } 
    pthread_mutex_unlock(&mutex);
}

int main() {
    printf("Este es mi PID: %d, utiliza kill -SIGUSR1 %d para cambiar de productor.\n", getpid(), getpid());
    printf("\n");
    sleep(3); // Espero 3 segundos.

    pthread_t productor1, productor2, consumidor_thread;

    signal(SIGUSR1, mane_signal); // Registra la función 'mane_signal' como manejador para la señal SIGUSR1

    pthread_mutex_init(&mutex, NULL); // Inicializa el mutex
    pthread_cond_init(&cond, NULL); // Inicializa la variable de condicion

    char p1_id = '1'; //Actuando productor 1
    char p2_id = '2'; //Actuando productor 2
    
    pthread_create(&productor1, NULL, productor, &p1_id); // Crea un hilo para el primer productor. Le pasa a la función 'productor' una referencia al ID del productor
    pthread_create(&productor2, NULL, productor, &p2_id); // Crea un hilo para el segundo productor. Le pasa a la función 'productor' una referencia al ID del productor
    pthread_create(&consumidor_thread, NULL, consumidor, NULL); // Crea un hilo para el consumidor

    pthread_join(productor1, NULL); // Espera a que termine el primer hilo productor
    pthread_join(productor2, NULL); // Espera a que termine el segundo hilo productor
    pthread_join(consumidor_thread, NULL); // Espera a que termine el hilo consumidor

    pthread_mutex_destroy(&mutex); // Destruye el mutex. Esto libera los recursos del sistema utilizados por el mutex
    pthread_cond_destroy(&cond); // Destruye la variable de condicion.

    return 0;
}
