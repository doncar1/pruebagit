#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>
#include <fcntl.h>  
#include <unistd.h> 

#define verde   "\x1b[32m"  // Definir códigos de escape ANSI para colores
#define magenta "\x1b[35m"
#define cyan    "\x1b[36m"
#define reset   "\x1b[0m"

#define DELAY 5e4       // 50 ms expresado en micras
//#define DELAY 5e6     // 500 ms expresado en micras
//#define DELAY 1000000 // 1 segundo expresado en micras

#define N 25    // Máximo número de lineas.
#define M 300   // Tamaño máximo de línea.

pthread_t P1, P2, C;                    //declaración de hilos
pthread_cond_t cond_cambio_productor;   //declaración de condiciones de espera
pthread_cond_t cond_leyo_matriz1;
pthread_cond_t cond_leyo_matriz2;
pthread_cond_t cond_escribio_matriz1;
pthread_cond_t cond_escribio_matriz2;

pthread_mutex_t mutex_turno;    //declaración de semaforos
pthread_mutex_t mutex_matriz1;
pthread_mutex_t mutex_matriz2;

int  matriz1_leida = 1;     //declaración de flags de condición de espera     
int  matriz2_leida = 1;
int  matriz1_escrita = 0;
int  matriz2_escrita = 0;
int  turno = 1; 
int  cambio = 0;

char matriz1[N][M];     // Buffer1 compartido
char matriz2[N][M];     // Buffer2 compartido

int trabajar = 1;       //flag para finalización de hilos

int fila = 0;           //variables para recorrer las matricez y los archivos    
int fila_aux = 0;              
int linea_actual = 0;
int i = 0;
int estoy_en_matriz1=1;

int cantidad_de_matricez=0;     //varibles para leer las ultimas lineas del archivo
int lineas_finales=0;


FILE *archivo;      //declaración de archivo de lectura y escritura
FILE *archivo2;


// Función del productor
void *productor1(void *arg) {

    while(trabajar){

        // Espero a que sea mi turno
        pthread_mutex_lock(&mutex_turno);
        while (turno != 1) {
                pthread_cond_wait(&cond_cambio_productor, &mutex_turno);
        }

        //reset de fila de la matriz
        //reset de fila auxiliar para el cambio de productores
        fila=0;
        if(fila_aux >= N){
            fila_aux=0;
        }
        fila=fila+fila_aux;
        
        //entra si estoy escribiendo en matriz1
        if(estoy_en_matriz1==1){

            //Espera a que consumidor lea la matriz1
            pthread_mutex_lock(&mutex_matriz1);        
            while (matriz1_leida != 1) {
                    pthread_cond_wait(&cond_leyo_matriz1, &mutex_matriz1);
            }

            //reset de flag de escritura
            if(turno==1){
                matriz1_escrita=0;
            }

            //guarda en las filas de matriz1 las lineas de texto, siempre que sea su turno
            while (fila + linea_actual < N + linea_actual && fgets(matriz1[fila], sizeof(matriz1[fila]), archivo) != NULL && turno == 1  ) {
                // Elimina el carácter de nueva línea si existe al final de la línea
                char *p = strchr(matriz1[fila], '\n');
                if (p != NULL) {
                    *p = '\0';
                }
                printf("P1 escribe en matriz 1, linea %d\n\n", fila+1);
                usleep(DELAY);
                fila++;              
                fila_aux++;         //guarda la fila de la matriz cuando se cambia de productor
                linea_actual++;     //guarda la linea actual del archivo
            } 

            //se sube el flag que indica la escritura completa de matriz1
            if(turno==1 || fila==N){
                matriz1_escrita=1;
            }

            //si termino de escribir matriz1, bajo el flag para indicar que estoy en matriz2
            if(fila==N){
                estoy_en_matriz1=0;
            }

            pthread_cond_signal(&cond_escribio_matriz1);    // Aviso que termine de llenar la matriz 1
            pthread_mutex_unlock(&mutex_matriz1);           
        }
        else{

            //Espera a que consumidor lea la matriz2
            pthread_mutex_lock(&mutex_matriz2);
            while (matriz2_leida != 1) {
                pthread_cond_wait(&cond_leyo_matriz2, &mutex_matriz2);
            }
            
            //reset de flag de escritura
            if(turno==1){
                matriz2_escrita=0;
            }

            //guarda en las filas de matriz2 las lineas de texto, siempre que sea su turno
            while (fila + linea_actual < N + linea_actual && fgets(matriz2[fila], sizeof(matriz2[fila]), archivo) != NULL && turno == 1) {
                // Elimina el carácter de nueva línea si existe al final de la línea
                char *p = strchr(matriz2[fila], '\n');
                if (p != NULL) {
                    *p = '\0';
                }
                printf(cyan "P1 escribe en matriz 2, linea %d\n\n" reset, fila+1);
                usleep(DELAY);
                fila++;
                fila_aux++;
                linea_actual++;
            } 

            //se sube el flag que indica la escritura completa de matriz2
            if(turno==1 || fila==N){
                matriz2_escrita=1;
            }

            //si termino de escribir matriz2, subo el flag para indicar que estoy en matriz1
            if(fila==N){
                estoy_en_matriz1=1;
            }

            pthread_cond_signal(&cond_escribio_matriz2);    // Aviso que termine de llenar la matriz 2
            pthread_mutex_unlock(&mutex_matriz2);           
        }
        

        pthread_cond_signal(&cond_cambio_productor);    //aviso que puedo cambiar de turno 
        pthread_mutex_unlock(&mutex_turno);             
        
    }

    pthread_exit(NULL);     //cuando trabajar es 0, retorno a join
}

// Función del productor
void *productor2(void *arg) {

    while(trabajar){

        // Espero a que sea mi turno
        pthread_mutex_lock(&mutex_turno);
        while (turno != 2) {
                pthread_cond_wait(&cond_cambio_productor, &mutex_turno);
        }

        //reset de fila de la matriz
        //reset de fila auxiliar para el cambio de productores
        fila=0;
        if(fila_aux >= N){
            fila_aux=0;
        }
        fila=fila+fila_aux;
        
        //entra si estoy escribiendo en matriz1
        if(estoy_en_matriz1==1){

            //Espera a que consumidor lea la matriz1
            pthread_mutex_lock(&mutex_matriz1);        
            while (matriz1_leida != 1) {
                    pthread_cond_wait(&cond_leyo_matriz1, &mutex_matriz1);
            }

            //reset de flag de escritura
            if(turno==2){
                matriz1_escrita=0;
            }

            //guarda en las filas de matriz1 las lineas de texto, siempre que sea su turno
            while (fila + linea_actual < N + linea_actual && fgets(matriz1[fila], sizeof(matriz1[fila]), archivo) != NULL && turno == 2  ) {
                // Elimina el carácter de nueva línea si existe al final de la línea
                char *p = strchr(matriz1[fila], '\n');
                if (p != NULL) {
                    *p = '\0';
                }
                printf("P2 escribe en matriz 1, linea %d\n\n", fila+1);
                usleep(DELAY);
                fila++;              
                fila_aux++;         //guarda la fila de la matriz cuando se cambia de productor
                linea_actual++;     //guarda la linea actual del archivo
            } 

            //se sube el flag que indica la escritura completa de matriz1
            if(turno==2 || fila==N){
                matriz1_escrita=1;
            }

            //si termino de escribir matriz1, bajo el flag para indicar que estoy en matriz2
            if(fila==N){
                estoy_en_matriz1=0;
            }

            pthread_cond_signal(&cond_escribio_matriz1);    // Aviso que termine de llenar la matriz 1
            pthread_mutex_unlock(&mutex_matriz1);           
        }
        else{

            //Espera a que consumidor lea la matriz2
            pthread_mutex_lock(&mutex_matriz2);
            while (matriz2_leida != 1) {
                pthread_cond_wait(&cond_leyo_matriz2, &mutex_matriz2);
            }
            
            //reset de flag de escritura
            if(turno==2){
                matriz2_escrita=0;
            }

            //guarda en las filas de matriz2 las lineas de texto, siempre que sea su turno
            while (fila + linea_actual < N + linea_actual && fgets(matriz2[fila], sizeof(matriz2[fila]), archivo) != NULL && turno == 2) {
                // Elimina el carácter de nueva línea si existe al final de la línea
                char *p = strchr(matriz2[fila], '\n');
                if (p != NULL) {
                    *p = '\0';
                }
                printf(cyan "P2 escribe en matriz 2, linea %d\n\n" reset, fila+1);
                usleep(DELAY);
                fila++;
                fila_aux++;
                linea_actual++;
            } 

            //se sube el flag que indica la escritura completa de matriz2
            if(turno==2 || fila==N){
                matriz2_escrita=1;
            }

            //si termino de escribir matriz2, subo el flag para indicar que estoy en matriz1
            if(fila==N){
                estoy_en_matriz1=1;
            }

            pthread_cond_signal(&cond_escribio_matriz2);    // Aviso que termine de llenar la matriz 2
            pthread_mutex_unlock(&mutex_matriz2);           
        }
        

        pthread_cond_signal(&cond_cambio_productor);    //aviso que puedo cambiar de turno 
        pthread_mutex_unlock(&mutex_turno);             
        
    }

    pthread_exit(NULL);     //caundo trabajar es 0, retorno a join
}


void *consumidor(void *arg) {

    while(trabajar){

        //Espera a que productor termine de llenar la matriz1
        pthread_mutex_lock(&mutex_matriz1);   
        while (matriz1_escrita != 1) {
                pthread_cond_wait(&cond_escribio_matriz1, &mutex_matriz1);
        }

        //reset flag de lectura
        matriz1_leida=0;
        
        //escribe en archivo2 las lineas que estan en matriz1 
        if(trabajar != 0){
            for (int i = 0; i < N ; i++) {
                printf(verde "C lee de matriz 1, línea %d: %s\n\n" reset, i + 1, matriz1[i]);
                fprintf(archivo2, "%s\n", matriz1[i]);
                usleep(DELAY);
            } 
        }

        //se sube el flag que indica que matriz 1, fue leida
        matriz1_leida=1;

        pthread_cond_signal(&cond_leyo_matriz1);    // Aviso que termine de leer la matriz1
        pthread_mutex_unlock(&mutex_matriz1);           

        //Espera a que productor termine de llenar la matriz2
        pthread_mutex_lock(&mutex_matriz2);   
        while (matriz2_escrita != 1) {
                pthread_cond_wait(&cond_escribio_matriz2, &mutex_matriz2);
        }
       
        //reset flag de lectura
        matriz2_leida=0;

        //escribe en archivo2 las lineas que estan en matriz2
        if(trabajar != 0){
            for (int i = 0; i < N ; i++) {
                printf(magenta "C lee de matriz 2, línea %d: %s\n\n" reset, i + 1, matriz2[i]);
                fprintf(archivo2, "%s\n", matriz2[i]);
                usleep(DELAY);
            }
        }

        //se sube el flag que indica que matriz 1, fue leida
        matriz2_leida=1;
        
        pthread_cond_signal(&cond_leyo_matriz2);    // Aviso que termine de llenar la matriz2
        pthread_mutex_unlock(&mutex_matriz2);           
    } 

    //codigo para leer lineas finales
    cantidad_de_matricez=linea_actual/25;       //calculo la cantidad de matricez que se escribieron

    if ((cantidad_de_matricez) % 2 == 0) {      //me fijo en que matriz escribio las ultimas lineas
        estoy_en_matriz1=1;
    } 

    lineas_finales= linea_actual-N*cantidad_de_matricez; //calculo la cantidad de lineas que se escrbieron en la matriz

    printf("lineas finales :%d\n", lineas_finales);
    printf("estoy matriz 1 :%d\n", estoy_en_matriz1);

    //leo lineas finales en la matriz correspondiente
    if(estoy_en_matriz1){

        for (int i = 0; i < lineas_finales ; i++) {
            printf(verde "C lee de matriz 1, línea %d: %s\n\n" reset, i + 1, matriz1[i]);
            fprintf(archivo2, "%s\n", matriz1[i]);
            usleep(DELAY);
        } 
    }
    else{
        
        for (int i = 0; i < lineas_finales; i++) {
            printf(magenta "C lee de matriz 2, línea %d: %s\n\n" reset, i + 1, matriz2[i]);
            fprintf(archivo2, "%s\n", matriz2[i]);
            usleep(DELAY);
        }
    }

    pthread_exit(NULL);
}

//función para cambiar de turno y envia señal para avisar el cambio
void cambiar_productor() 
{
   if (turno == 1) {
        turno = 2;
    } else {
        turno = 1;
    }
    pthread_cond_signal(&cond_cambio_productor);
    
}

//manejador de la señal SIGUSR1, que levanta el flag cambio
void SignalHandler(int sig) {
    printf("\nSeñal SIGUSR1 recibida. Intercambiando proceso productor.\n\n");
    cambio=1;       
}


int main() {

        //inicialización de variables de condición
        pthread_cond_init(&cond_escribio_matriz1, NULL);
        pthread_cond_init(&cond_escribio_matriz1, NULL);
        pthread_cond_init(&cond_escribio_matriz2, NULL);
        pthread_cond_init(&cond_leyo_matriz1, NULL);
        pthread_cond_init(&cond_leyo_matriz2, NULL);

        //inicialización de semaforos
        pthread_mutex_init(&mutex_turno, NULL);
        pthread_mutex_init(&mutex_matriz1, NULL);
        pthread_mutex_init(&mutex_matriz2, NULL);
      

        //enlazar SIGUSR1 con el manejador de señales
        signal(SIGUSR1, SignalHandler);

        //crear hilos de los productores
        pthread_create(&P1, NULL, productor1, NULL);
        pthread_create(&P2, NULL, productor2, NULL);

        //crear hilo consumidor.
        pthread_create(&C, NULL, consumidor, NULL);

        //abrir archivo de entrada
        archivo = fopen("pg2001.txt", "r");  

        if (archivo == NULL) {
            perror("No se pudo abrir el archivo");
            exit(1);
        }

        //abrir archivo de salida
        archivo2 = fopen("datos_leidos.txt", "w");

        if (!archivo2) {
            perror("Error al abrir el archivo");
            exit(2);
        }

        //recorre el while hasta que se llega al final del archivo, el flag trabajar se hace cero y los hilos finalizan sus tareas 
        while(trabajar){
            if(feof(archivo)){
                trabajar=0;
            }
            //cuando llega SIGURS1, se llama a cambio de productor y se resetea el flag cambio.
            if(cambio==1){
                cambiar_productor();
                cambio=0;
            }
        }

        // Esperar a que los hilos terminen
        pthread_join(C, NULL);
        
        //cambia de productor para que los dos productores finalicen
        cambiar_productor();

        pthread_join(P1, NULL);
        pthread_join(P2, NULL);
        
        //cierro ambos archivos
        fclose(archivo);
        printf("cerro archivo\n");
        fclose(archivo2);
        printf("cerro archivo2\n");

        //destruyo semaforos y variables de condición
        pthread_cond_destroy(&cond_cambio_productor);
        pthread_cond_destroy(&cond_escribio_matriz1);
        pthread_cond_destroy(&cond_escribio_matriz2);
        pthread_cond_destroy(&cond_leyo_matriz1);
        pthread_cond_destroy(&cond_escribio_matriz2);

        pthread_mutex_destroy(&mutex_turno);
        pthread_mutex_destroy(&mutex_matriz1);
        pthread_mutex_destroy(&mutex_matriz2);
        
        return 0;
}