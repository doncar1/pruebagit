#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <fcntl.h>  // Para O_RDONLY
#include <unistd.h> // Para read y close

// Definir códigos de escape ANSI para colores
#define verde   "\x1b[32m"
#define magenta "\x1b[35m"
#define cyan    "\x1b[36m"
#define reset   "\x1b[0m"

#define DELAY 5e4               // 50 ms expresado en micras
//#define DELAY 5e6               // 500 ms expresado en micras
//#define DELAY 1000000           // 1 segundo expresado en micras

#define N 25 // Máximo número de lineas.
#define M 300 // Tamaño máximo de línea.

pthread_t P1, P2, C;
pthread_cond_t cond_cambio_productor;
pthread_cond_t cond_leyo_matriz1;
pthread_cond_t cond_leyo_matriz2;
pthread_cond_t cond_escribio_matriz1;
pthread_cond_t cond_escribio_matriz2;

pthread_mutex_t mutex_turno;
pthread_mutex_t mutex_matriz1;
pthread_mutex_t mutex_matriz2;

int  matriz1_leida = 1;
int  matriz2_leida = 1;
int  matriz1_escrita = 0;
int  matriz2_escrita = 0;
int  turno = 1; 
int  cambio = 0;

char matriz1[N][M];             // Buffer1 compartido
char matriz2[N][M];             // Buffer2 compartido

int trabajar = 1;

int fila = 0;               
int fila_aux = 0;              
int linea_actual = 0;
int i = 0;

int cantidad_de_matricez=0;
int estoy_en_matriz1=1;
int lineas_finales=0;
int final_archivo=0;

FILE *archivo;
FILE *archivo2;


// Función del productor
void *productor1(void *arg) {

    while(trabajar){

        // Espero a mi turno
        pthread_mutex_lock(&mutex_turno);
        while (turno != 1) {
                pthread_cond_wait(&cond_cambio_productor, &mutex_turno);
        }

        //Espera a que consumidor lea la matriz1
        pthread_mutex_lock(&mutex_matriz1);        
        while (matriz1_leida != 1 && estoy_en_matriz1==1) {
                pthread_cond_wait(&cond_leyo_matriz1, &mutex_matriz1);
        }

        //reset de filas
        fila=0;
        if(fila_aux >= N){
            fila_aux=0;
        }
        fila=fila+fila_aux;

        //reset de flag de escritura
        if(turno==1 && estoy_en_matriz1==1){
            matriz1_escrita=0;
        }

        while (fila + linea_actual < N + linea_actual && fgets(matriz1[fila], sizeof(matriz1[fila]), archivo) != NULL && turno == 1 && estoy_en_matriz1==1 ) {
            // Elimina el carácter de nueva línea si existe al final de la línea
            char *p = strchr(matriz1[fila], '\n');
            if (p != NULL) {
                *p = '\0';
            }
            printf("P1 escribe en matriz 1, linea %d\n\n", fila+1);
            usleep(DELAY);
            fila++;
            fila_aux++;
            linea_actual++;    
        } 

        if(turno==1 && estoy_en_matriz1==1){
            matriz1_escrita=1;
        }

        if(fila==N){
            estoy_en_matriz1=0;
        }

        pthread_cond_signal(&cond_escribio_matriz1);    // Aviso que termine de llenar la matriz 1
        pthread_mutex_unlock(&mutex_matriz1);           // Desbloqueo la zona critica 1
        
        //Espera a que consumidor lea la matriz2
        pthread_mutex_lock(&mutex_matriz2);
        while (matriz2_leida != 1 && estoy_en_matriz1==0) {
            pthread_cond_wait(&cond_leyo_matriz2, &mutex_matriz2);
        }
        

        //reset de filas
        fila=0;
        if(fila_aux >= N){
            fila_aux=0;
        }
        fila=fila+fila_aux;

        //reset de flag de escritura
        if(turno==1 && estoy_en_matriz1==0){
            matriz2_escrita=0;
        }

        while (fila + linea_actual < N + linea_actual && fgets(matriz2[fila], sizeof(matriz2[fila]), archivo) != NULL && turno == 1 && estoy_en_matriz1==0) {
            // Elimina el carácter de nueva línea si existe al final de la línea
            char *p = strchr(matriz2[fila], '\n');
            if (p != NULL) {
                *p = '\0';
            }
            printf(cyan "P1 escribe en matriz 2, linea %d\n\n" reset, fila+1);
            usleep(DELAY);
            fila++;
            fila_aux++;
            linea_actual++;
        } 

        if(turno==1 && estoy_en_matriz1==0){
            matriz2_escrita=1;
        }

        if(fila==N){
            estoy_en_matriz1=1;
        }

        cantidad_de_matricez=linea_actual/25;

        pthread_cond_signal(&cond_escribio_matriz2);    // Aviso que termine de llenar la matriz 2
        pthread_mutex_unlock(&mutex_matriz2);           // Desbloqueo la zona critica 2


        pthread_cond_signal(&cond_cambio_productor);    // 
        pthread_mutex_unlock(&mutex_turno);             // 
        
    }
   

    if ((cantidad_de_matricez) % 2 == 0) {
        estoy_en_matriz1=1;
    } 

    lineas_finales= linea_actual-N*cantidad_de_matricez;

    final_archivo=1;

    printf("lineas finales :%d\n", lineas_finales);
    printf("es en matriz 1 :%d\n", estoy_en_matriz1);

    printf("cerro archivo\n");
    fclose(archivo);
    pthread_exit(NULL);
}

void *productor2(void *arg) {

        while(trabajar){

        // Espero a mi turno
        pthread_mutex_lock(&mutex_turno);
        while (turno != 2) {
                pthread_cond_wait(&cond_cambio_productor, &mutex_turno);
        }

        //Espera a que consumidor lea la matriz1
        pthread_mutex_lock(&mutex_matriz1);        
        while (matriz1_leida != 1 && estoy_en_matriz1 == 1) {
            pthread_cond_wait(&cond_leyo_matriz1, &mutex_matriz1);
            printf("entre al while 1\n");
        }

        //reset de filas
        fila=0;
        if(fila_aux >= N){
            fila_aux=0;
        }
        fila=fila+fila_aux;

        //reset de flag de escritura
        if(turno==2 && estoy_en_matriz1==1){
            matriz1_escrita=0;
        }

        while (fila + linea_actual < N + linea_actual && fgets(matriz1[fila], sizeof(matriz1[fila]), archivo) != NULL && turno == 2 && estoy_en_matriz1==1) {
            // Elimina el carácter de nueva línea si existe al final de la línea
            char *p = strchr(matriz1[fila], '\n');
            if (p != NULL) {
                *p = '\0';
            }
            printf("P2 escribe en matriz 1, linea %d\n\n", fila+1);
            usleep(DELAY);
            fila++;
            fila_aux++;
            linea_actual++;    
        } 

        if(turno==2 && estoy_en_matriz1==1){
            matriz1_escrita=1;
        }

        if(fila==N){
            estoy_en_matriz1=0;
        }
        pthread_cond_signal(&cond_escribio_matriz1);    // Aviso que termine de llenar la matriz 1
        pthread_mutex_unlock(&mutex_matriz1);           // Desbloqueo la zona critica 1
        
        //Espera a que consumidor lea la matriz2
        pthread_mutex_lock(&mutex_matriz2);
        while (matriz2_leida != 1 && estoy_en_matriz1==0) {
           pthread_cond_wait(&cond_leyo_matriz2, &mutex_matriz2);
           printf("entre al while 2\n");
        }
        

        //reset de filas
        fila=0;
        if(fila_aux >= N){
            fila_aux=0;
        }
        fila=fila+fila_aux;

        //reset de flag de escritura
        if(turno==2 && estoy_en_matriz1==0){
            matriz2_escrita=0;
        }

        while (fila + linea_actual < N + linea_actual && fgets(matriz2[fila], sizeof(matriz2[fila]), archivo) != NULL && turno == 2 && estoy_en_matriz1==0) {
            // Elimina el carácter de nueva línea si existe al final de la línea
            char *p = strchr(matriz2[fila], '\n');
            if (p != NULL) {
                *p = '\0';
            }
            printf(cyan "P2 escribe en matriz 2, linea %d\n\n" reset, fila+1);
            usleep(DELAY);
            fila++;
            fila_aux++;
            linea_actual++;
        } 

         if(turno==2 && estoy_en_matriz1==0){
            matriz2_escrita=1;
        }

        if(fila==N){
            estoy_en_matriz1=1;
        }

        cantidad_de_matricez=linea_actual/25;

        pthread_cond_signal(&cond_escribio_matriz2);    // Aviso que termine de llenar la matriz 2
        pthread_mutex_unlock(&mutex_matriz2);           // Desbloqueo la zona critica 2
        

        pthread_cond_signal(&cond_cambio_productor);    // 
        pthread_mutex_unlock(&mutex_turno);             // 
        
    }
   

    if ((cantidad_de_matricez) % 2 == 0) {
        estoy_en_matriz1=1;
    } 

    lineas_finales= linea_actual-N*cantidad_de_matricez;

    final_archivo=1;

    printf("lineas finales :%d\n", lineas_finales);
    printf("es en matriz 1 :%d\n", estoy_en_matriz1);

    printf("cerro archivo\n");
    fclose(archivo);
    pthread_exit(NULL);
}


void *consumidor(void *arg) {

    while(trabajar){
        //Espera a que productor termine de llenar la matriz1
        pthread_mutex_lock(&mutex_matriz1);   
        while (matriz1_escrita != 1) {
                pthread_cond_wait(&cond_escribio_matriz1, &mutex_matriz1);
        }

        matriz1_leida=0;
        
        for (int i = 0; i < N && final_archivo == 0 ; i++) {
            printf(verde "C lee de matriz 1, línea %d: %s\n\n" reset, i + 1, matriz1[i]);
            fprintf(archivo2, "%s\n", matriz1[i]);
            usleep(DELAY);
        } 

        matriz1_leida=1;

        pthread_cond_signal(&cond_leyo_matriz1);    // Aviso que termine de llenar la matriz 1
        pthread_mutex_unlock(&mutex_matriz1);           // Desbloqueo la zona critica 1

        //Espera a que productor termine de llenar la matriz1
        pthread_mutex_lock(&mutex_matriz2);   
        while (matriz2_escrita != 1) {
                pthread_cond_wait(&cond_escribio_matriz2, &mutex_matriz2);
        }
       
        matriz2_leida=0;

        for (int i = 0; i < N && final_archivo==0; i++) {
            printf(magenta "C lee de matriz 2, línea %d: %s\n\n" reset, i + 1, matriz2[i]);
            fprintf(archivo2, "%s\n", matriz2[i]);
            usleep(DELAY);
        }
     
        matriz2_leida=1;
        
        pthread_cond_signal(&cond_leyo_matriz2);    // Aviso que termine de llenar la matriz 1
        pthread_mutex_unlock(&mutex_matriz2);           // Desbloqueo la zona critica 1
    } 

    while(final_archivo==0){

    }

    if(estoy_en_matriz1){

        for (int i = 0; i < lineas_finales ; i++) {
            printf(verde "C lee de matriz 1, línea %d: %s\n\n" reset, i + 1, matriz1[i]);
            fprintf(archivo2, "%s\n", matriz1[i]);
            sleep(1);
            //usleep(1e5);
        } 
    }
    else{
        
        for (int i = 0; i < lineas_finales; i++) {
            printf(magenta "C lee de matriz 2, línea %d: %s\n\n" reset, i + 1, matriz2[i]);
            fprintf(archivo2, "%s\n", matriz2[i]);
            sleep(1);
            //usleep(1e5);
        }
    }

    printf("cerro archivo2\n");
    fclose(archivo2); 
    pthread_exit(NULL);
}

void cambiar_productor() 
{
   if (turno == 1) {
        turno = 2;
    } else {
        turno = 1;
    }
    pthread_cond_signal(&cond_cambio_productor);
    pthread_cond_signal(&cond_leyo_matriz1);
    pthread_cond_signal(&cond_leyo_matriz2);
}


void SignalHandler(int sig) {
    printf("\nSeñal SIGUSR1 recibida. Intercambiando proceso productor.\n\n");
    cambio=1;       
}


int main() {

        pthread_cond_init(&cond_escribio_matriz1, NULL);
        pthread_cond_init(&cond_escribio_matriz1, NULL);
        pthread_cond_init(&cond_escribio_matriz2, NULL);
        pthread_cond_init(&cond_leyo_matriz1, NULL);
        pthread_cond_init(&cond_leyo_matriz2, NULL);

        pthread_mutex_init(&mutex_turno, NULL);
        pthread_mutex_init(&mutex_matriz1, NULL);
        pthread_mutex_init(&mutex_matriz2, NULL);
      

        // Signal Handler.
        signal(SIGUSR1, SignalHandler);

        // Crear hilos de los productores
        pthread_create(&P1, NULL, productor1, NULL);
        pthread_create(&P2, NULL, productor2, NULL);

        // Crear hilo consumidor.
        pthread_create(&C, NULL, consumidor, NULL);

        archivo = fopen("pg2001.txt", "r");  

        if (archivo == NULL) {
            perror("No se pudo abrir el archivo");
            exit(1);
        }

        archivo2 = fopen("datos_leidos.txt", "w");

        if (!archivo2) {
            perror("Error al abrir el archivo");
            exit(2);
        }

        while(trabajar){
            if(feof(archivo)){
                trabajar=0;
            }
            if(cambio==1){
                cambiar_productor();
                cambio=0;
            }
        }

        // Esperar a que los hilos terminen
        pthread_join(P1, NULL);
        pthread_join(P2, NULL);
        pthread_join(C, NULL);

        pthread_cond_destroy(&cond_cambio_productor);
        pthread_cond_destroy(&cond_escribio_matriz1);
        pthread_cond_destroy(&cond_escribio_matriz2);
        pthread_cond_destroy(&cond_leyo_matriz1);
        pthread_cond_destroy(&cond_escribio_matriz2);

        pthread_mutex_destroy(&mutex_turno);
        pthread_mutex_destroy(&mutex_matriz1);
        pthread_mutex_destroy(&mutex_matriz2);
        

        return 0;
}