#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <fcntl.h>  // Para O_RDONLY
#include <unistd.h> // Para read y close


#define N 25 // Máximo número de lineas.
#define M 300 // Tamaño máximo de línea.

pthread_t P1, P2, C;
pthread_cond_t cond;
pthread_cond_t cond2;
pthread_cond_t cond3;
pthread_mutex_t mutex;
pthread_mutex_t mutex2;
pthread_mutex_t mutex3;

int  producir = 1;
int  esperar_consumir=0;
int  cambio_turno=0;
int  estoy_en_matriz1 = 1;
char matriz1[N][M];             // Buffer1 compartido
char matriz2[N][M];             // Buffer2 compartido
int turno = 1;                  // Inicialmente, el productor 1 comienza
int trabajar = 1;
int fila =0;               
int fila_aux=0;              
int linea_actual=0;
int linea_actual2=0;
int i=0;
FILE *archivo;
FILE *archivo2;


// Función del productor
void *productor1(void *arg) {
    
    while(trabajar){

        pthread_mutex_lock(&mutex);

        while (turno != 1) {
                pthread_cond_wait(&cond, &mutex);
        }

        pthread_mutex_lock(&mutex3);
        
        while (esperar_consumir != 0) {
                pthread_cond_wait(&cond3, &mutex3);
        }

        fila=0;
        if(fila_aux >= N){
            fila_aux=0;
        }
        fila=fila+fila_aux;
        
        if (archivo == NULL) {
            perror("No se pudo abrir el archivo");
            exit(1);
        }

        

        if(estoy_en_matriz1==1){

            while (fila + linea_actual < N + linea_actual && fgets(matriz1[fila], sizeof(matriz1[fila]), archivo) != NULL && turno == 1 ) {
                // Elimina el carácter de nueva línea si existe al final de la línea
                char *p = strchr(matriz1[fila], '\n');
                if (p != NULL) {
                    *p = '\0';
                }
                printf("P1 escribe en matriz 1, linea %d: %s\n\n", fila+1, matriz1[fila]);
                sleep(1);
                fila++;
                fila_aux++;
                linea_actual++;    
            } 

            if(turno==1|| fila==N){    
                estoy_en_matriz1=0;
            }
        }
        else{
            while (fila + linea_actual < N + linea_actual && fgets(matriz2[fila], sizeof(matriz2[fila]), archivo) != NULL && turno == 1 ) {
                // Elimina el carácter de nueva línea si existe al final de la línea
                char *p = strchr(matriz2[fila], '\n');
                if (p != NULL) {
                    *p = '\0';
                }
                printf("P1 escribe en matriz 2, linea %d: %s\n\n", fila+1, matriz2[fila]);
                sleep(1);
                fila++;
                fila_aux++;
                linea_actual++;
            } 

            if(turno==1 || fila==N){    
                estoy_en_matriz1=1;
            }

            if(cambio_turno==0){
            esperar_consumir=1;
            }
            cambio_turno=0;
        }
        
        if(cambio_turno==0){
            producir=0;
        }
        cambio_turno=0;

        pthread_cond_signal(&cond);
        pthread_mutex_unlock(&mutex); 
        
        pthread_cond_signal(&cond2);
        pthread_mutex_unlock(&mutex2);

        pthread_cond_signal(&cond3);
        pthread_mutex_unlock(&mutex3); 

    }
    fclose(archivo);  // Cierra el archivo
    pthread_exit(NULL);
    return 0;
}

void *productor2(void *arg) {

       while(trabajar){

        pthread_mutex_lock(&mutex);

        while (turno != 2) {
                pthread_cond_wait(&cond, &mutex);
        }

        pthread_mutex_lock(&mutex3);
        
        while (esperar_consumir != 0) {
                pthread_cond_wait(&cond3, &mutex3);
        }

        fila=0;
        if(fila_aux >= N){
            fila_aux=0;
        }
        fila=fila+fila_aux;
        
        if (archivo == NULL) {
            perror("No se pudo abrir el archivo");
            exit(1);
        }

        

        if(estoy_en_matriz1==1){

            while (fila + linea_actual < N + linea_actual && fgets(matriz1[fila], sizeof(matriz1[fila]), archivo) != NULL && turno == 2 ) {
                // Elimina el carácter de nueva línea si existe al final de la línea
                char *p = strchr(matriz1[fila], '\n');
                if (p != NULL) {
                    *p = '\0';
                }
                printf("P2 escribe en matriz 1, linea %d: %s\n\n", fila+1, matriz1[fila]);
                sleep(1);
                fila++;
                fila_aux++;
                linea_actual++;    
            } 

            if(turno==2|| fila==N){    
                estoy_en_matriz1=0;
            }
        }
        else{
            while (fila + linea_actual < N + linea_actual && fgets(matriz2[fila], sizeof(matriz2[fila]), archivo) != NULL && turno == 2 ) {
                // Elimina el carácter de nueva línea si existe al final de la línea
                char *p = strchr(matriz2[fila], '\n');
                if (p != NULL) {
                    *p = '\0';
                }
                printf("P2 escribe en matriz 2, linea %d: %s\n\n", fila+1, matriz2[fila]);
                sleep(1);
                fila++;
                fila_aux++;
                linea_actual++;
            } 

            if(turno==2 || fila==N){    
                estoy_en_matriz1=1;
            }

            if(cambio_turno==0){
            esperar_consumir=1;
            }
            cambio_turno=0;
        }
        
        if(cambio_turno==0){
            producir=0;
        }
        cambio_turno=0;

        pthread_cond_signal(&cond);
        pthread_mutex_unlock(&mutex); 
        
        pthread_cond_signal(&cond2);
        pthread_mutex_unlock(&mutex2);

        pthread_cond_signal(&cond3);
        pthread_mutex_unlock(&mutex3); 
    }
        printf("jeje");
    fclose(archivo);  // Cierra el archivo
    pthread_exit(NULL);
    return 0;
}


void *consumidor(void *arg) {

    while(trabajar){

        pthread_mutex_lock(&mutex2);
        
        while (producir != 0) {
                pthread_cond_wait(&cond2, &mutex2);
        }

        pthread_cond_signal(&cond2);
        pthread_mutex_unlock(&mutex2);
        
        if (!archivo2) {
            perror("Error al abrir el archivo");
            exit(2);
        }

        if(estoy_en_matriz1==0){
            for (int i = 0; i < N ; i++) {
                printf("C lee de matriz 1, línea %d: %s\n\n", i + 1, matriz1[i]);
                fprintf(archivo2, "%s\n", matriz1[i]);
                sleep(1);
            } 
        }
        else{
            for (int i = 0; i < N; i++) {
                printf("C lee de matriz 2, línea %d: %s\n\n", i + 1, matriz2[i]);
                fprintf(archivo2, "%s\n", matriz2[i]);
                sleep(1);
            }

            esperar_consumir=0;
            producir=1;
        } 

        

        pthread_cond_signal(&cond3);
        pthread_mutex_unlock(&mutex3); 
        
    }
    fclose(archivo2); 
    pthread_exit(NULL);
}

void cambiar_productor() 
{
   if (turno == 1) {
        turno = 2;
    } else {
        turno = 1;
    }
    pthread_cond_signal(&cond);
}


void SignalHandler(int sig) {
    switch (sig) {
        case SIGUSR1:
                printf("Señal SIGUSR1 recibida. Intercambiando proceso productor.\n\n");
                cambio_turno=1;
                cambiar_productor();
                break;
        case SIGTERM:   
                printf("Señal SIGTERM recibida. Terminando el proceso. \n\n");
                trabajar = 0;
                pthread_cond_signal(&cond);
                pthread_cond_signal(&cond2);
                pthread_cond_signal(&cond3);
                break;
        // Se puede agregar otra señal mas si es necesario.
    }
}


int main() {

        pthread_cond_init(&cond, NULL);
        pthread_mutex_init(&mutex, NULL);
        pthread_cond_init(&cond2, NULL);
        pthread_mutex_init(&mutex2, NULL);
        pthread_cond_init(&cond3, NULL);
        pthread_mutex_init(&mutex3, NULL);


        // Signal Handler.
        signal(SIGUSR1, SignalHandler);
        signal(SIGTERM, SignalHandler);

        // Crear hilos de los productores
        pthread_create(&P1, NULL, productor1, NULL);
        pthread_create(&P2, NULL, productor2, NULL);

        // Crear hilo consumidor.
        pthread_create(&C, NULL, consumidor, NULL);

        archivo = fopen("pg2000.txt", "r");  
        archivo2 = fopen("datos_leidos.txt", "w");

        // Esperar a que los hilos terminen
        pthread_join(P1, NULL);
        pthread_join(P2, NULL);
        pthread_join(C, NULL);

        pthread_cond_destroy(&cond);
        pthread_mutex_destroy(&mutex);
        pthread_cond_destroy(&cond2);
        pthread_mutex_destroy(&mutex2);
        pthread_cond_destroy(&cond3);
        pthread_mutex_destroy(&mutex3);

        return 0;
}