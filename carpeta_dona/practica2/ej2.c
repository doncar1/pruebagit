#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main() {
	pid_t pid;
	char programa[100];  // Variable para almacenar el nombre del programa ingresado por el usuario

	printf("Ingresa el nombre del programa a ejecutar: ");
	scanf("%s", programa);

	// Crear un nuevo proceso hijo
	pid = fork();

	if (pid < 0) {
		// Error al crear el proceso hijo
		perror("Fallo fork");
		exit(1);
	} else if (pid == 0) {
		// Este es el código que se ejecuta en el proceso hijo

		// execl() toma la ruta del programa a ejecutar y una lista de argumentos como parametros.
		if (execl(programa, programa, NULL) == -1) {
			perror("Error al ejecutar el programa");
		exit(1);
	}
		// Terminar el proceso hijo
		exit(0);
	} else {
		// Este es el código que se ejecuta en el proceso padre
		// Esperar a que el proceso hijo termine
		int status;
		waitpid(pid, &status, 0);

		if (WIFEXITED(status)) {
			printf("El proceso hijo terminó con estado: %d\n", WEXITSTATUS(status));
		} else {
			printf("El proceso hijo terminó de manera anormal.\n");
		}

		}

	return 0;
}
