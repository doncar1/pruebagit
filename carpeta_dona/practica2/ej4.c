#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/wait.h>

int var=1;
int nuevo_hijo=0;
int numHijos=0;
int terminarhijos=0;
int capacidadHijos=0; //Capacidad Dinámica de hijos.
pid_t *hijos = NULL; // Arreglo dinámico para almacenar los PIDs de los hijos

// Función para agregar un PID al arreglo dinámico de hijos
void agregarHijo(pid_t pid) {
	if (numHijos >= capacidadHijos) {
	// Si el arreglo está lleno, aumenta su capacidad
	capacidadHijos += 10; // Agregamos 10 a la capacidad máxima.
	hijos = (pid_t*)realloc(hijos, capacidadHijos * sizeof(pid_t));

	if (hijos == NULL) {
		perror("Error al reallocar el arreglo de PIDs");
		exit(EXIT_FAILURE);
		}
	}

	hijos[numHijos++] = pid;
}



void signal_handler(int signum) {

    switch (signum) {
	case SIGUSR1:
	printf("Señal SIGUSR1 recibida, con identificador %d \n",signum);
	printf("Creando un proceso hijo... \n");
	nuevo_hijo=1;
	break;

        case SIGUSR2:
	printf("Señal SIGUSR2 recibida, con identificador %d \n",signum);
	printf("Terminando los hijos...\n");
	terminarhijos=1;
	break;


        case SIGALRM:
	printf("\n\n\nSoy el proceso Padre con PID %d y cree %d procesos hijos. \n\n\n",getpid(),numHijos);
        alarm(10);
	break;

        default:
	printf("Señal no manejada: %d\n", signum);
	break;
    }
}

int main() {
	int exit=0;

	signal(SIGUSR1, signal_handler);
	signal(SIGUSR2, signal_handler);
	signal(SIGALRM, signal_handler);
	alarm(10);

	while(exit==0){

		if(var==0){  //Sentencias del hijo.
			printf("Soy un proceso hijo con PID %d \n",getpid());
			printf("El PID de mi padre es: %d \n",getppid());
			sleep(5);
		}
		else{	//Sentencias del padre.


		if(nuevo_hijo==1){
			var=fork(); // Crea un nuevo hijo.
			agregarHijo(var);
			nuevo_hijo=0;
		}


		if(terminarhijos==1){
			// Itera a través de los PIDs de los hijos y envía SIGTERM para terminarlos
			for (int i = 0; i < numHijos; i++) {
				kill(hijos[i], SIGTERM);
				waitpid(hijos[i], NULL, 0);	// Esperamos para evitar que se conviertan en procesos zombie.
    				}

			// Libera la memoria del arreglo de PIDs
			free(hijos);
			numHijos = 0;
			capacidadHijos=0;
			terminarhijos=0;
			printf("Todos los procesos hijos terminados satisfactoriamente.\n");
			}

		}


	}


    return 0;
}
