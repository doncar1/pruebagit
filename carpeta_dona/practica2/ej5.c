#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>


// Estas variables no las va a heredar el hijo, por mas que sean globales. Hay que usar memoria compartida.
int variableGlobal1 = 0;
int variableGlobal2 = 0;
int childflag=0;
pid_t childPid;

// Signal handler para ambas señales
void handler(int signum) {
    switch (signum) {
        case SIGUSR1:
            variableGlobal1 += 1;
            break;
        case SIGUSR2:
            variableGlobal2 += 1;
            break;
	case SIGINT:
		childflag=1;
        default:
            break;
    }
}

int main() {
// Defino variables locales.
int variableLocal=0;

    // Registramos el signal handler para ambas señales
    signal(SIGUSR1, handler);
    signal(SIGUSR2, handler);
signal(SIGINT, handler);

    // Creamos un proceso hijo
    childPid = fork();

    if (childPid == -1) {
        perror("Error al crear el proceso hijo");
        exit(1);
    }

    if (childPid == 0) {
        // Este es el proceso hijo
        while (1) {
            // Espera a recibir la señal para imprimir el estado de las variables globales
            sleep(1);
		if(childflag==1){
		printf("Proceso hijo: variableGlobal1 = %d, variableGlobal2 = %d\n", variableGlobal1, variableGlobal2);
		printf("Proceso hijo: variableLocal = %d \n", variableLocal);
		}
		childflag=0;
            }
    } else {
        // Este es el proceso padre
        while (1) {
		if(variableGlobal1 !=variableLocal){
		variableLocal=variableGlobal1;
		}
            // Espera a recibir la señal para imprimir el estado de las variables globales
            sleep(1);
            printf("Proceso padre: variableGlobal1 = %d, variableGlobal2 = %d\n", variableGlobal1, variableGlobal2);
		printf("Proceso padre: variableLocal = %d \n", variableLocal);

            // Enviamos una señal al proceso hijo para solicitar la impresión
            kill(childPid, SIGINT);

            // Esperamos un segundo para que el proceso hijo imprima y actualice sus variables
            sleep(1);
        }
    }

    return 0;
}
