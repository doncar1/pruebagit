#include <stdio.h>	// Para operaciones de entrada/salida estándar
#include <stdlib.h>	// Para funciones de gestión de memoria y otras utilidades
#include <sys/ipc.h>	// Para funciones relacionadas con IPC (Inter-Process Communication)
#include <sys/shm.h>	// Para funciones relacionadas con la memoria compartida
#include <unistd.h>	// Para funciones del sistema operativo
#include <signal.h>	// Para el manejo de señales
#include <stdbool.h>	// Para el uso de variables booleanas.

// Declaracion de las funciones principales.
void descripcion(void);
void proceso_A(void);
void proceso_B(void);
void encriptar(int *datos, int size);
void desencriptar(int *datos, int size);

// Declaracion de variables globales.
pid_t pid_hijo;
bool work = true;
bool wait1 = true;
bool wait2 = true;
int *mem_comp;
char father;
char son;

void signalhandler(int signum) {
	switch (signum) {
		case SIGTERM:
			printf("Señal SIGTERM recibida.\n");
			if (pid_hijo == 0){// La recibió el proceso hijo, es un escenario posible.
				if (son == 'A'){
					printf("Señal recibida por el proceso A. Terminando los procesos.\n");
					work = false;
				}
				else {
					printf("Señal recibida por el proceso B. Debe enviarla al proceso A.\n");
				}
			}
			else {		// La recibió el proceso padre, es otro escenario posible.
				if (father == 'A'){
					printf("Señal recibida por el proceso A. Terminando los procesos.\n");
					work = false;
				}
				else {
					printf("Señal recibida por el proceso B. Debe enviarla al proceso A.\n");
				}
			}
			break;
		case SIGUSR1:
			//printf("Señal SIGUSR1 recibida.\n");
			wait1 = false;
			break;
		case SIGUSR2:
			wait2 = false;
			break;
		default:
			printf("Señal no manejada\n");
			break;
		}
}


void encriptar(int *datos, int size){
	int i;
	for(i=0; i < size; i++){
		datos[i]= (4 * datos[i] + 5) % 27;
	}
}


void desencriptar(int *datos, int size){
	int i;
	for(i=0; i < size; i++){
		datos[i]= (7 * datos[i] + 19) % 27;
	}
}


void proceso_A(void){
	int datosA[] = {0,1,2,3,4,5,6,7,8,9};
	// Encriptados son: {5,9,13,17,21,25,2,6,10,14}
	int sizeA = sizeof(datosA)/sizeof(datosA[0]);
	encriptar(datosA,sizeA);
	int i;	// Escribir en memoria compartida.
	for(i=0; i<10; i++){
		mem_comp[i]=datosA[i];
		sleep(1);	// La escritura se hace a razón de 1 elemento por segundo.
	}

	while(wait1){	// Esperando que el proceso B termine de escribir.
	}
	wait1 = true;	// Reiniciamos la espera para la proxima iteracion.

	// Ahora le respondemos al proceso B que puede continuar.
	if(pid_hijo == 0){ // El proceso A resultó ser hijo. Hay que enviarla al padre.
		kill(getppid(),SIGUSR1);
	}
	else{	// El proceso A resultó ser padre. Hay que enviarla al hijo.
		kill(pid_hijo,SIGUSR1);
	}

	// Sale del bucle cuando ya puede leer los datos. Los copiamos al vector auxiliar A.
	for(i=0; i<10; i++){
		datosA[i]=mem_comp[i+10];
	}
	desencriptar(datosA,sizeA);	// Desencriptamos.

	printf("Los datos leidos por el proceso A con PID %d son:\n",getpid());
	printf("[");
	for(i=0; i<9; i++){
		printf("%d, ",datosA[i]);
	}
	printf("%d]\n",datosA[9]);

	// Ahora le avisamos al proceso B que puede imprimir en pantalla.
	if(pid_hijo == 0){ // El proceso A resultó ser hijo. Hay que enviarla al padre.
		kill(getppid(),SIGUSR2);
	}
	else{	// El proceso A resultó ser padre. Hay que enviarla al hijo.
		kill(pid_hijo,SIGUSR2);
	}
}

void proceso_B(void){
	int datosB[] = {10,11,12,13,14,15,16,17,18,19};
	// Encriptados son: {18,22,26,3,7,11,15,19,23,0}
	int sizeB = sizeof(datosB)/sizeof(datosB[0]);
	encriptar(datosB,sizeB);
	int i;	// Escribir en memoria compartida.
	for(i=0; i<10; i++){
		mem_comp[i+10]=datosB[i];
		sleep(1);	// La escritura se hace a razón de 1 elemento por segundo.
	}

	while(wait1){
		sleep(1); // Le da tiempo al proceso A a terminar.
		// Avisamos al proceso A que ya terminamos de escribir mediante SIGUSR1.
		if(pid_hijo == 0){ // El proceso B resultó ser hijo. Hay que enviarla al padre.
			kill(getppid(),SIGUSR1);
		}
		else{	// El proceso B resultó ser padre. Hay que enviarla al hijo.
			kill(pid_hijo,SIGUSR1);
		}
	}	// Va a salir del bucle cuando el proceso A le envie SIGUSR1 en respuesta.
	wait1 = true;	// Reiniciamos la espera para la proxima iteracion.

	// Luego lee los 10 primeros elementos desencriptandolos.
	for(i=0; i<10; i++){
		datosB[i]=mem_comp[i];
	}
	desencriptar(datosB,sizeB);

	while(wait2){
		// Esperamos a que el proceso A imprima primero en pantalla.
	}
	wait2=true;	// Reiniciamos la espera para la proxima iteracion.

	printf("Los datos leidos por el proceso B con PID %d son:\n",getpid());
	printf("[");
	for(i=0; i<9; i++){
		printf("%d, ",datosB[i]);
	}
	printf("%d ]\n",datosB[9]);
}


int main(int argc, char *argv[]) {

/*	VERIFICAR ARGUMENTOS DE ENTRADA		*/

if (argc != 2){		// No se introdujeron argumentos.
	descripcion();
	return 1;
}

if (*argv[1] == 'a') {
	// Padre es tipo A e hijo es tipo B
	descripcion();
	father = 'A';
	son = 'B';
} else if (*argv[1] == 'b') {
	// Padre es tipo B e hijo es tipo A
	descripcion();
	father = 'B';
	son = 'A';
} else {
	// Argumentos inválidos.
	descripcion();
	return 1;
}

//Inicializar el manejador de señales.
signal(SIGTERM, signalhandler);
signal(SIGUSR1, signalhandler);
signal(SIGUSR2, signalhandler);
// Inicializando memoria compartida.

key_t key = ftok("/bin/",12345);
int shm_id = shmget(key, 20*sizeof(int),IPC_CREAT | 0666);
mem_comp = (int *)shmat(shm_id, NULL, 0);

pid_hijo=fork();	// El proceso hijo puede ser A o B. Es indiferente.

// Bucle principal
while(work){
	if(pid_hijo==0)
	{// Proceso hijo
		if(son=='A')
		{// El proceso hijo es A
		proceso_A();
		}
		else
		{// El proceso hijo es B
		proceso_B();
		}
	}
	else
	{// Proceso Padre
		if(father=='A')
		{// El proceso padre es A
		proceso_A();
		}
		else
		{// El proceso padre es B
		proceso_B();
		}
	}
}

// Solo el proceso A puede salir del bucle principal al recibir SIGTERM.

int kill_B;
if (pid_hijo == 0){ // El proceso hijo resultó ser A
	// Hay que mandar SIGKILL al proceso padre.
	kill_B = kill(getppid(), SIGKILL);
	if (kill_B == 0){
		printf("Proceso B terminado satisfactoriamente.\n");
	}
	else {
		perror("Error al enviar SIGKILL.\n");
	}
}
else { // El proceso padre resultó ser A
	// Hay que mandar SIGKILL al proceso hijo.
	kill_B = kill(pid_hijo, SIGKILL);
	if (kill_B == 0){
		printf("Proceso B terminado satisfactoriamente.\n");
	}
	else {
		perror("Error al enviar SIGKILL.\n");
	}

}

// Liberando recursos compartidos.
shmdt(mem_comp);
shmctl(shm_id, IPC_RMID, NULL);

printf("Recursos compartidos liberados satisfactoriamente.\n");
printf("Terminando el proceso A.\n");

return 0;
}


void descripcion(void){
printf("Trabajo Práctico 3: Uso de memoria compartida\n");
printf("Ejercicio 6. Grupo 12.\n");
printf("Descripción:\n");
printf("El proceso actual posee  PID %d.\n",getpid());
printf("El proceso crea a un proceso hijo. Ambos procesos comparten un espacio de memoria del tamaño de 20 enteros.\n");
printf("El proceso A escribe los primeros 10 valores y B se encarga de leerlos.\n");
printf("El proceso B escribe los últimos 10 valores y A se encarga de leerlos.\n");
printf("El proceso debe repetirse hasta que A recibe la señal SIGTERM.\n");
printf("Cuando esto ocurre, se terminan  ambos procesos liberando los recursos compartidos.\n");
printf("\n");
printf("Los datos son numeros entre 0 y 26, representando letras entre la 'a' y la 'z'.\n");
printf("Y  deben ser cifrados antes de ser escritos mediante la siguiente funcion de cifrado:\n");
printf("\n");
printf("f(x) = (4x + 5) mod 27\n");
printf("\n");
printf("Y para el decifrado se utiliza la siguiente funcion.\n");
printf("\n");
printf("f^(-1)(x) = (7x + 19) mod 27\n");
printf("\n");
printf("Los parametros (4,5) son claves publicas disponibles en la memoria compartida y (7,19) son claves privadas\n");
printf("conocidas solo por el proceso lector.\n");
printf("La escritura se realiza a razón de un valor por segundo.\n");
printf("La lectura se realiza tan pronto como se puede, imprimiendo el PID del proceso lector.\n");
printf("El programa debe ser único\n");
printf("Es decir, puede actuar como proceso A o proceso B dependiendo de un parametro que se envie por linea de comando.\n");
printf("\n");
printf("Como usar el programa:\n");
printf("Ejecute el programa en la linea de comando utilizando './Ej6 a' o './Ej6 b'.\n");
printf("Si no introduce el argumento 'a' o 'b' el programa solo imprime este mensaje.\n");
}
