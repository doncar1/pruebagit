#include <stdio.h>      // Para operaciones de entrada/salida estándar
#include <stdlib.h>     // Para funciones de gestión de memoria y otras utilidades
#include <sys/ipc.h>    // Para funciones relacionadas con IPC (Inter-Process Communication)
#include <sys/shm.h>    // Para funciones relacionadas con la memoria compartida
#include <unistd.h>     // Para funciones del sistema operativo
#include <signal.h>     // Para el manejo de señales
#include <stdbool.h>    // Para el uso de variables booleanas.

// Definicion de variables globales.
bool work = true;
bool wait1 = true;
bool wait2 = true;
int *mem_comp;
pid_t *pid_comp;
char rol_global;


void signalhandler(int signum) {
        switch (signum) {
                case SIGTERM:
			if(rol_global=='a'){
				printf("Señal SIGTERM recibida por proceso A. Terminando programa\n");
				work = false;
			} else {
				printf("Señal SIGTERM recibida por proceso B. Enviar al proceso A.\n");
			} break;
                case SIGUSR1:
                        //printf("Señal SIGUSR1 recibida.\n");
                        wait1 = false;
                        break;
                case SIGUSR2:
			//printf("Señal SIGUSR2 recibida\n");
                        wait2 = false;
                        break;
                default:
                        printf("Señal no manejada\n");
                        break;
                }
}

void encriptar(int *datos, int size,int a,int b){
        int i;
        for(i=0; i < size; i++){
                datos[i]= (a * datos[i] + b) % 27;
        }
}


void desencriptar(int *datos, int size){
// Claves privadas conocidas solo por el proceso lector.
int k=7;
int c=19;

        int i;
        for(i=0; i < size; i++){
                datos[i]= (k * datos[i] + c) % 27;
        }
}


int almacenar_PID(pid_t pid, pid_t *pid_array, char rol) {
if(rol == 'a'){
	pid_array[0] = pid;
	return 1;
} else {
	pid_array[1] = pid;
	return 0;
	}
}



int main(int argc, char *argv[]) {

/* VERIFICACION DE ARGUMENTOS */
if (argc != 2) {
	printf("El programa requiere un argumento 'a' o 'b'.\n");
	return 1;
}

char rol = argv[1][0];	// Accede al primer caracter del primer argumento.

if (rol == 'a') {
	printf("El programa debe ser tipo A.\n");
	rol_global='a';
} else if (rol == 'b') {
	printf("El prograMa debe ser tipo B.\n");
	rol_global='b';
} else {
	printf("Argumento inválido: %c\n", rol);
	return 1;
}

/* INICIALIZACION DEL MANEJADOR DE SEÑALES */
signal(SIGTERM, signalhandler);
signal(SIGUSR1, signalhandler);
signal(SIGUSR2, signalhandler);

/* CREACION DE LA MEMORIA COMPARTIDA DE 20 ELEMENTOS*/
key_t key = ftok("/bin/",12345);
int shm_id = shmget(key, 22*sizeof(int),IPC_CREAT | 0666);
mem_comp = (int *)shmat(shm_id, NULL, 0);

/* CLAVES PUBLICAS EN MEMORIA COMPARTIDA */
mem_comp[20]=4;
mem_comp[21]=5;

/* CREACION DE LA MEMORIA COMPARTIDA DE PIDS */
key_t key2 = ftok("/bin/",54321);
int shm_id2 = shmget(key2, 2*sizeof(pid_t),IPC_CREAT | 0666);
pid_comp = (pid_t *)shmat(shm_id2, NULL, 0);

/* ALMACENAMOS EL PID EN UN LUGAR ESPECIFICO. SIEMPRE SOBREESCRIBE. */
int resultado=almacenar_PID(getpid(), pid_comp, rol);
if(resultado == 1){
	printf("Programa ocupó el primer elemento\n");
} else {
	printf("Programa ocupó el segundo elemento\n");
}
printf("Estado del vector de PIDS: [%d,%d]\n",pid_comp[0],pid_comp[1]);

int datosA[] = {0,1,2,3,4,5,6,7,8,9}; // Encriptados son: {18,22,26,3,7,11,15,19,23,0}
int datosB[] = {10,11,12,13,14,15,16,17,18,19};	// Encriptados son: {18,22,26,3,7,11,15,19,23,0}
int size = sizeof(datosA)/sizeof(datosA[0]);
int i;
/* BUCLE PRINCIPAL */
while(work){

	/* LA PRIMER ETAPA ES ESCRIBIR */

	if(rol=='a'){
		encriptar(datosA,size,mem_comp[20],mem_comp[21]);
		for(i=0;i<10;i++){
			mem_comp[i]=datosA[i];
			sleep(1);	// Escritura a razon de un elemento por segundo.
		}
		printf("Espero a que B termine de escribir para continuar.\n");
		while(wait1){
			//Esperar a que B termine de escribir para continuar.
		}
		wait1=true; //Reiniciar wait1 para la proxima iteracion.
		printf("Terminé de escribir, el proceso B ya puede leer.\n");
		kill(pid_comp[1],SIGUSR1); // Avisamos a B que terminó de escribir.
		for(i=0;i<10;i++){	//Leer los ultimos 10 elementos.
			datosA[i]=mem_comp[i+10];
		}

	} else {
		encriptar(datosB,size,mem_comp[20],mem_comp[21]);
		for(i=0;i<10;i++){
			mem_comp[i+10]=datosB[i];
			sleep(1);	// Escritura a razon de un elemento por segundo.
		}
		while(wait1){
			sleep(1);	// Le damos algo de tiempo de espera.
			kill(pid_comp[0],SIGUSR1);	//Avisar que terminó de escribir.
		} wait1=true;
		printf("Terminé de escribir, el proceso A ya puede leer.\n");

		for(i=0;i<10;i++){	//Leer los primeros 10 elementos.
			datosB[i]=mem_comp[i];
		}
	}

	/* LA SEGUNDA ETAPA ES LEER */

	if(rol=='a'){	// Primero lee el programa A
		desencriptar(datosA,size);
		printf("Primero imprime en pantalla el proceso A.\n");
		printf("Los datos leidos por el proceso A con PID %d son:\n",getpid());
	        printf("[");
	        for(i=0; i<9; i++){
	                printf("%d, ",datosA[i]);
	        }
	        printf("%d]\n",datosA[9]);
		printf("Le avisamos al proceso B que es su turno de imprimir.\n");
	        kill(pid_comp[1],SIGUSR2);	// Ahora le avisamos al proceso B que puede imprimir en pantalla
	}
	else {
		desencriptar(datosB,size);
		printf("El proceso B espera que sea su turno de imprimir.\n");
		while(wait2){
			sleep(1);
			// El proceso B comienza bloqueado, esperando a que le envien SIGUSR2.
		} wait2=true;
		printf("Ya es el turno del proceso B de imprimir.\n");
		printf("Los datos leidos por el proceso B con PID %d son:\n",getpid());
	        printf("[");
	        for(i=0; i<9; i++){
	                printf("%d, ",datosB[i]);
	        }
	        printf("%d ]\n",datosB[9]);
	}
}

// Solo el proceso A va a salir del bucle principal.
kill(pid_comp[1],SIGKILL);
printf("Proceso B terminado satisfactoriamente.\n");

// Liberando recursos compartidos.
shmdt(mem_comp);
shmdt(pid_comp);
shmctl(shm_id, IPC_RMID, NULL);
shmctl(shm_id2, IPC_RMID, NULL);

printf("Recursos compartidos liberados satisfactoriamente.\n");
printf("Terminando el proceso A.\n");

return 0;
}
