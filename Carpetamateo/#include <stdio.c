#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_USERNAME_LENGTH 60
#define MAX_EMAIL_LENGTH 40

// Estructura para almacenar información de usuarios
typedef struct {
    char username[MAX_USERNAME_LENGTH + 1];
    unsigned int userID;
    char email[MAX_EMAIL_LENGTH + 1];
} UserData;

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Uso: %s <nombre_archivo_binario>\n", argv[0]);
        return 1;
    }

UserData user;
    strcpy(user.email, "");
    unsigned int userID = 1;  // ID de usuario inicial

    printf("Ingrese la información de los usuarios:\n");

    while (1) {
        printf("Nombre de Usuario: ");
        if (scanf("%s", user.username) != 1) {
            printf("Error al leer el nombre de usuario.\n");
            break;
        }

        if (strcmp(user.username, "") == 0) {
            // Usuario ingresó nombre vacío, finalizar programa
            break;
        }

        user.userID = userID++;

        printf("e-mail: ");
        if (scanf("%s", user.email) != 1) {
            printf("Error al leer el e-mail.\n");
            break;
        }

        // Escribir la estructura en el archivo binario
        fwrite(&user, sizeof(UserData), 1, file);
    }

    fclose(file);
    printf("Información almacenada en %s\n", argv[1]);

    return 0;
}