#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>


int signalA=0;
int signalB=0;
int signalC=0;

pid_t *hijosA = NULL;	// Arreglo dinámico con los PID de los hijos tipo A.
int capacidadhijos=0;	// Capacidad dinámica de hijos.
int numhijosA=0;

int finish=0;

// Función para agregar un PID al arreglo dinámico de hijos
void agregarhijo(pid_t pid) {
	if (numhijosA >= capacidadhijos) {
	// Si el arreglo está lleno, aumenta su capacidad
	capacidadhijos += 10; // Agregamos 10 a la capacidad máxima.
	hijosA = (pid_t*)realloc(hijosA, capacidadhijos * sizeof(pid_t));

	if (hijosA == NULL) {
		perror("Error al reallocar el arreglo de PIDs");
		exit(EXIT_FAILURE);
		}
	}

	hijosA[numhijosA++] = pid;
}

// Función que actuará como manejador de señal
void signalhandler(int signum) {
	switch (signum) {
		case SIGUSR1:
			printf("Señal SIGUSR1 recibida.\n");
			signalA=1;
			break;
		case SIGUSR2:
			printf("Señal SIGUSR2 recibida.\n");
			signalB=1;
			break;
		case SIGTERM:
			printf("Señal SIGTERM recibida.\n");
			signalC=1;
			break;
		default:
			printf("Señal no manejada: %d\n", signum);
	}
}

int main() {
//Inicializar manejador de señales.
signal(SIGUSR1, signalhandler);
signal(SIGUSR2, signalhandler);
signal(SIGTERM, signalhandler);

int pidhijoA;
int pidhijoB;
int main_loop=1;

printf("soy el padre y mi PID es %d \n",getpid());
printf("ingrese una de estas señales para realizar la acción deseada:\n");
printf("SIGUSR1: crea un hijo e imprime en pantalla PID del padre y del hijo creado cada 5 segundos\n");
printf("SIGUSR2: crea un hijo que realiza un ls del directorio donde se ejecuta el codigo\n");
printf("SIGTERM: termina todos los procesos hijos creados con SIGUSR1 cada 1 segundo y por ultimo termina el proceso padre\n");

while (main_loop) {



	if (signalA==1){
		//Crea un hijo tipo A cada vez que recibe SIGUSR1.
		pidhijoA=fork();
		agregarhijo(pidhijoA);	// Agregamos el PID al vector de hijos.
		signalA=0;
	}

	if (signalB==1){
		//Crea un hijo tipo B cada vez que recibe SIGUSR2.
		pidhijoB=fork();
		signalB=0;
	}

	if (signalC==1){
		//Termina todos los procesos creados por SIGUSR1. Debe hacerlos a razón de 1 por segundo.
		finish=1;
		signalC=0;
	}



	if (pidhijoA == 0){	// Proceso hijo tipo A
		while(1){
			printf("Soy un proceso hijo tipo A con PID %d y el PID de mi padre es %d \n",getpid(),getppid());
			sleep(5);
		}
	}

	if (pidhijoB == 0){	// Proceso hijo tipo B

		char *program = "/bin/ls"; // Ruta al programa.
		char *argumentos[] = {program, "-l", NULL}; // Argumentos

		printf("Soy un proceso hijo tipo B con PID %d \n",getpid());
		printf("Voy a ejecutar el programa ls que reside en bin\n");

		// Se ejecuta el programa reemplazando al proceso hijo.
		if (execvp(program,argumentos) == -1){
			perror("Error al ejecutar el programa");
			exit(1);
		}

	}

	if(finish==1){
		// Itera a través de los PIDs de los hijos y envía SIGKILL para terminarlos
		for (int i = 0; i < numhijosA; i++) {
			printf("Terminando el proceso hijo con PID %d \n",hijosA[i]);
			kill(hijosA[i], SIGKILL);	//SIGKILL No puede ser ignorada.
			waitpid(hijosA[i], NULL, 0); // Esperamos para evitar que se conviertan en procesos zombie.
			sleep(1);	// Esperamos 1 segundo entre terminaciones.
		}

		// Libera la memoria del arreglo de PIDs
		free(hijosA);
		numhijosA = 0;
		capacidadhijos=0;
		finish=0;
		printf("Todos los procesos hijos terminados satisfactoriamente.\n");
		main_loop=0;	//Terminamos el proceso padre.
		}

	}



printf("Terminando el proceso padre con PID %d \n",getpid());
return 0;
}

