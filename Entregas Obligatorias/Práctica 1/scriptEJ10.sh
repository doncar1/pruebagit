#! /bin/sh

#verifica si me paso un argumento
if [ ! -d $1 ]; then
	echo "el argumento ingresado no es un directorio"
exit 1
fi
    directorio=$1
    nombredirectorio=$(basename $directorio)
    DirNum=0
    FileNum=0
    NotFile=0
    for item in "$directorio"/*; do  #analiza progresivamente objeto por objeto
        if [ -f "$item" ] && [ ! -L "$item" ]; then  #se ve si es un archivo y no un enlance
            FileNum=$((FileNum + 1))
        elif [ -d "$item" ]; then #si no es el caso, se ve si es un directorio
            DirNum=$((DirNum + 1)) 
        else
            NotFile=$((NotFile + 1)) #si no es ninguno de los anteriores, es un archivo no regular
        fi
    done
    echo "el directorio a analizar es: $nombredirectorio"
    echo "El número de directorios es: $DirNum"
    echo "El número de archivos regulares es: $FileNum"
    echo "El número de archivos no regulares es: $NotFile"
exit 0
