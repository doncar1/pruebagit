Script de la práctica 1.
Grupo 12.

Funcionalidad:
Cuenta la cantidad de archivos regulares, directorios y archivos no regulares
de un determinado directorio.

Se considera archivo regular a todos aquellos archivos
 caracterizados por un '-' bajo el estándar UNIX.

Se considera directorio a todos aquellos archivos caracterizados por 'd'
bajo el estándar UNIX.

Se considera no regular a todos aquellos archivos que no cumpla alguna de
estas dos caracteristicas.

El script no cuenta archivos ocultos.

Modo de uso:
Reproducir el script mediante el comando bash e introduzca la ruta del
directorio que desea analizar.

Ejemplo:
bash scriptEJ10.sh ./Práctica 1/Carpeta de prueba/
